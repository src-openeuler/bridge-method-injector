Name:                bridge-method-injector
Version:             1.20
Release:             1
Summary:             Evolve Java classes without breaking compatibility
License:             MIT
URL:                 https://github.com/infradna/bridge-method-injector
Source0:             https://github.com/infradna/bridge-method-injector/archive/bridge-method-injector-parent-%{version}.tar.gz
Source1:             LICENSE.txt
BuildArch:           noarch
BuildRequires:       maven-local mvn(junit:junit) mvn(org.apache.maven:maven-plugin-api)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-release-plugin)
BuildRequires:       mvn(org.apache.maven.scm:maven-scm-provider-gitexe)
BuildRequires:       mvn(org.jenkins-ci:annotation-indexer) mvn(org.ow2.asm:asm-all)
BuildRequires:       mvn(org.apache.maven.plugins:maven-plugin-plugin)
%description
This package contains small Java library for generating
synthetic bridge methods with different return types
to help backward compatibility.

%package -n bridge-method-annotation
Summary:             Bridge method injection annotations
%description -n bridge-method-annotation
This package contains annotations for injecting bridge methods.

%package        javadoc
Summary:             Javadoc for %{name}
%description    javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}-%{name}-parent-%{version}
cp %{SOURCE1} LICENSE
%mvn_package :bridge-method-annotation bridge-method-annotation
%pom_xpath_remove "pom:extension[pom:artifactId[text()='wagon-svn']]" injector
%pom_remove_plugin :nexus-staging-maven-plugin
%pom_change_dep :asm-debug-all :asm-all injector

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE

%files -n bridge-method-annotation -f .mfiles-bridge-method-annotation
%doc LICENSE

%files javadoc -f .mfiles-javadoc
%doc LICENSE

%changelog
* Mon Dec 09 2024 Ge Wang <wang__ge@126.com> - 1.20-1
- update to version 1.20

* Fri Aug 28 2020 Anan Fu <fuanan3@huawei.com> - 1.14-1
- package init
